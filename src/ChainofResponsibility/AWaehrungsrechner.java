package ChainofResponsibility;

import java.util.ArrayList;

public abstract class AWaehrungsrechner {

   protected AWaehrungsrechner aWaehrungsrechner;
   protected ArrayList<AWaehrungsrechner> waehrungsrechner;


   public void setaWaehrungsrechner(AWaehrungsrechner aWaehrungsrechner){
       this.aWaehrungsrechner = aWaehrungsrechner;
   }


   abstract public void handleRequest(Request request);


}
