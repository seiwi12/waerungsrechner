package ChainofResponsibility;

public class Client {

    public static void main(String[] args){
        AWaehrungsrechner dollar2Eur = new Dollar2Eur();
        AWaehrungsrechner dollar2Yen = new Dollar2Yen();
        AWaehrungsrechner dollar2Pfund = new Dollar2Pfund();

        dollar2Eur.setaWaehrungsrechner(dollar2Pfund);
        dollar2Yen.setaWaehrungsrechner(dollar2Eur);
        dollar2Pfund.setaWaehrungsrechner(dollar2Yen);

        dollar2Eur.handleRequest(new Request(120.00,"Dollar2Pfund"));


    }
}
