package ChainofResponsibility;

public class Dollar2Eur extends AWaehrungsrechner {

    @Override
    public void handleRequest(Request request) {
        if (request.getWährungsrechnername().equals("Dollar2Eur")){
            double neuerwert = request.getWert()*0.88;
            System.out.println(request.getWert()+" Dollar sind "+neuerwert+ " Euro");
        }else if (aWaehrungsrechner !=null){
            aWaehrungsrechner.handleRequest(request);
        }
    }
}
