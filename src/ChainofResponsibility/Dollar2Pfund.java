package ChainofResponsibility;

public class Dollar2Pfund extends AWaehrungsrechner{

    @Override
    public void handleRequest(Request request) {
        if (request.getWährungsrechnername().equals("Dollar2Pfund")){
            double neuerwert = request.getWert()*0.79;
            System.out.println(request.getWert()+" Dollar sind "+neuerwert+ " Pfund Sterling");
        }else if (aWaehrungsrechner !=null){
            aWaehrungsrechner.handleRequest(request);
        }
    }
}
