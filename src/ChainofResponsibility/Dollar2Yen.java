package ChainofResponsibility;

public class Dollar2Yen extends AWaehrungsrechner {
    @Override
    public void handleRequest(Request request) {
        if (request.getWährungsrechnername().equals("Dollar2Yen")){
            double neuerwert = request.getWert()*108.53;
            System.out.println(request.getWert()+" Dollar sind "+neuerwert+" Yen");
        }else if (aWaehrungsrechner !=null){
            aWaehrungsrechner.handleRequest(request);

        }
    }
}
