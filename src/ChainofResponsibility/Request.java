package ChainofResponsibility;

public class Request {
    private double wert;
    private String Währungsrechnername;

    public Request(double wert, String währungsrechnername) {
        this.wert = wert;
        Währungsrechnername = währungsrechnername;
    }

    public void setWert(double wert) {
        this.wert = wert;
    }

    public void setWährungsrechnername(String währungsrechnername) {
        Währungsrechnername = währungsrechnername;
    }

    public double getWert() {
        return wert;
    }

    public String getWährungsrechnername() {
        return Währungsrechnername;
    }
}
