package com.company;

public interface ISammelUmrechnung {
    public double sammelumrechnen(double[] betraege, String variante);
}
